void kiir (Csomopont * elem, std::ostream & os)
{
    if (elem != NULL)
    {
        ++melyseg;

        kiir (elem->egyesGyermek (), os);

        kiir (elem->nullasGyermek (), os);
        
        for (int i = 0; i < melyseg; ++i)
            os << "---";
        os << elem->getBetu () << "(" << melyseg - 1 << ")" << std::endl;

        --melyseg;
    }
}