n <- 0
door <- c(1,2,3)
for ( i in 1:10000) {
    cardoor <- sample(door,1)
    select <- sample(door,1)
    remove <- ifelse(cardoor==select, 
                    sample(setdiff(door,cardoor),1),
                    setdiff(door,c(cardoor,select)))
    reselect <- setdiff(door,c(select,remove))
    if (cardoor == reselect) {
        n <- n + 1
    }
}
print(n/10000)