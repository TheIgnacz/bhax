#include <stdio.h>
int main() {
	int x,y,t,*a,*b;
	
	printf("Adj meg két számot\n");
	scanf("%d%d", &x, &y);
	printf("Megcserélés előtt: %d, %d\n", x, y);
	
	a = &x;
	b = &y;
	
	t = *b;
	*b = *a;
	*a = t;
	printf("Csere után: %d, %d\n", x, y);
	
	return 0;
}