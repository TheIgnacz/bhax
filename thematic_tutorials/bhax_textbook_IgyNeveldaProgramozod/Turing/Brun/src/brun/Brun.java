/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brun;

/**
 *
 * @author theig
 */
import java.util.Scanner;
public class Brun {

    static double sum = 0.0;
    static int i, f;
    //Megnézi hogy prím-e a szám
    public static boolean isPrime(int n)
    {
        if (n % 2== 0) return false;
        for(i = 3 ;i * i <= n;i += 2) {
            if(n % i ==0)
                return false;
        }
        return true;

    }
    
    // Megnézi hogy két szám ikerprím-e, és kiszámolja a Brun állandót.
    public static void twinPrimesBrunConstant(int number )
    {
        for (int i = 2; i < number ; i++)
        {
            int num1 = i;
            int num2 = i + 2;
            if (isPrime(num1) && isPrime(num2))
            {
                System.out.println(num1 + " " + num2);
                sum = sum + (double)1/num1 + (double)1/num2;
            }
        }
    }
    public static void main(String[] args)
    {
        int no;
        Scanner scanner = new Scanner(System.in);
        System.out.print("Írj be egy számot: ");
        no = scanner.nextInt();
        twinPrimesBrunConstant(no);
        System.out.println("Brun állandó:" + sum);
    }
}
