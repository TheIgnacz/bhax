/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package montyhall;

/**
 *
 * @author Máté
 */
public class MontyHall {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int lefutásDB = 10000000;  // Próbák száma
        int győzelemDB = 0;   // Győzelmek száma

        for (int i = 0; i < lefutásDB; i++) {

            // A nyereményt elrejtjük 3 ajtó mögött. (0, 1, 2)
            int nyeremény = (int) (3 * Math.random());

            // Kiválasztunk egy ajtót.
            int választott = (int) (3 * Math.random());

            // Kinyitunk egy ajtót ami nem a választott és nyeremény.
            int kiNyit;
            do {
                kiNyit = (int) (3 * Math.random());
            } while ((kiNyit == választott) || (kiNyit == nyeremény));

            // Megcseréljük a választásunkat.
            int csere = 0 + 1 + 2 - kiNyit - választott;

            // Megnézzünk hogy az új választott ajó nyer-e.
            if (csere == nyeremény) {
                győzelemDB++;
            }
        }
        
        System.out.println("Próbák száma: " + lefutásDB);
        System.out.println("Győzelmek száma: " + győzelemDB);
        System.out.println("Esély hogy a cserélés után nyerünk: " + (1.0 * győzelemDB / lefutásDB) * 100 + "%");
    }

}
