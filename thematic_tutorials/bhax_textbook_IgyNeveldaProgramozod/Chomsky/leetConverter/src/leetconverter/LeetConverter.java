/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leetconverter;

import java.util.Scanner;

/**
 *
 * @author theig
 */
public class LeetConverter {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String str = sc.nextLine();
        System.out.println("Bemenet String: " + str);
        System.out.println("Kódoln(0) vagy dekódolni(1) szeretnéd");
        int option = sc.nextInt();
        if (option == 0){
            encode(str);
        }else if(option == 1) {
            decode(str);
        }
        
    }
    public static void encode(String text){
        text = text.toLowerCase();
        text = text.replaceAll("e", "3");
        text = text.replaceAll("a", "4");
        text = text.replaceAll("s", "5");
        text = text.replaceAll("i", "1");
        text = text.replaceAll("o", "0");
        text = text.replaceAll("g", "6");
        text = text.replaceAll("t", "7");
        System.out.println("LeetSpeak: " + text);
    }
    
    public static void decode(String text){
        text = text.toLowerCase();
        text = text.replaceAll("3", "e");
        text = text.replaceAll("4", "a");
        text = text.replaceAll("5", "s");
        text = text.replaceAll("1", "i");
        text = text.replaceAll("0", "o");
        text = text.replaceAll("6", "g");
        text = text.replaceAll("7", "t");
        System.out.println("Normális szöveg: " + text);
    }
    
}
