/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xorencryption;

/**
 *
 * @author theig
 */
public class XOREncryption {

    /**
     * @param args the command line arguments
     */
        // The same function is used to encrypt and 
    // decrypt 
    static String encryptDecrypt(String inputString) 
    { 
        // Define XOR key 
        // Any character value will work 
        char xorKey = '0'; 
  
        // Define String to store encrypted/decrypted String 
        String outputString = ""; 
  
        // calculate length of input string 
        int len = inputString.length(); 
  
        // perform XOR operation of key 
        // with every caracter in string 
        for (int i = 0; i < len; i++)  
        { 
            outputString = outputString +  
            Character.toString((char) (inputString.charAt(i) ^ xorKey)); 
        } 
  
        System.out.println(outputString); 
        return outputString; 
    } 
  
    // Driver code 
    public static void main(String[] args) 
    { 
        String sampleString = "Test"; 
  
        // Encrypt the string 
        System.out.println("Encrypted String"); 
        String encryptedString = encryptDecrypt(sampleString); 
  
        // Decrypt the string 
        System.out.println("Decrypted String"); 
        encryptDecrypt(encryptedString); 
    }
    
}
